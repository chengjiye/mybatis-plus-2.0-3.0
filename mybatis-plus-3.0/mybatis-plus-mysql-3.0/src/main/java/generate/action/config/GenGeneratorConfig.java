package generate.action.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * 默认的代码生成的配置
 *
 * @author Cheng JiYe
 * @date 2020/3/22 18:30
 */
public class GenGeneratorConfig extends AbstractGeneratorConfig {
    protected void globalConfig() {
        // 写自己项目的绝对路径,注意具体到java目录
        globalConfig.setOutputDir("D:\\projects");
        globalConfig.setFileOverride(true);
        globalConfig.setEnableCache(false);
        globalConfig.setBaseResultMap(true);
        globalConfig.setBaseColumnList(true);
        globalConfig.setOpen(false);
        globalConfig.setAuthor("User");
    }

    protected void dataSourceConfig() {
        dataSourceConfig.setDbType(DbType.MYSQL);
        dataSourceConfig.setDriverName("com.mysql.cj.jdbc.Driver");
        dataSourceConfig.setUsername("root");
        dataSourceConfig.setPassword("root");
        dataSourceConfig.setUrl("jdbc:mysql://localhost:3306/mycs?characterEncoding=utf8");
    }

    protected void strategyConfig() {
        // 此处可以修改为您的表前缀
        strategyConfig.setTablePrefix(new String[]{""});
        strategyConfig.setNaming(NamingStrategy.underline_to_camel);
    }

    protected void packageConfig() {
        packageConfig.setParent(null);
        packageConfig.setEntity("cn.mycs.cms.modular.system.model");
        packageConfig.setMapper("cn.mycs.cms.modular.system.dao");
        packageConfig.setXml("cn.mycs.cms.modular.system.dao.mapping");
    }

    protected void contextConfig() {
        contextConfig.setProPackage("cn.mycs.cms");
        contextConfig.setCoreBasePackage("cn.mycs.core");
        contextConfig.setBizChName("字典管理");
        contextConfig.setBizEnName("sysDict");
        contextConfig.setModuleName("system");
        // 写自己项目的绝对路径
        contextConfig.setProjectPath("D:\\projects\\mycs\\cms");
        contextConfig.setEntityName("SysDict");
        // 这里写已有菜单的名称,当做父节点

        // region mybatis-plus 生成器开关
        contextConfig.setEntitySwitch(true);
        contextConfig.setDaoSwitch(true);
        contextConfig.setServiceSwitch(true);
        // endregion

        // region 代码生成器开关
        contextConfig.setControllerSwitch(true);
        contextConfig.setIndexPageSwitch(true);
        contextConfig.setAddPageSwitch(true);
        contextConfig.setEditPageSwitch(true);
        contextConfig.setJsSwitch(true);
        contextConfig.setInfoJsSwitch(true);
        contextConfig.setSqlSwitch(true);
        // endregion
    }

    @Override
    protected void config() {
        globalConfig();
        dataSourceConfig();
        strategyConfig();
        packageConfig();
        contextConfig();
    }
}
