package com.hy.oms.controller.${context.catalogue};

import com.hy.oms.service.${context.catalogue}.I${context.bizChNameTmp}Service;
import com.hy.oms.entity.${context.catalogue}.${context.entityName};
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.hy.oms.vo.ResponseResult;
import com.hy.oms.utils.Const;
import java.util.List;
import java.util.ArrayList;
import com.hy.oms.utils.StringUtils;
import org.springframework.util.CollectionUtils;
import com.hy.oms.base.BaseService;
import com.hy.oms.base.RequestPageHead;
import lombok.extern.slf4j.Slf4j;


/**
 * @author ${controller.author}
 * @description ${context.bizChName}控制器
 * @date ${context.dateController}
 */
@Api(tags = "${context.bizChName}接口")
@RestController
@RequestMapping("/${context.bizEnName}/")
@Slf4j
public class ${context.bizEnBigName}Controller {

    @Autowired
    private I${context.bizChNameTmp}Service ${context.bizEnName}Service;

    @ApiOperation(value = "获取${context.bizChName}列表")
    @PostMapping("list")
    public ResponseResult list(@RequestBody RequestPageHead req) {
        log.debug("${context.bizEnBigName}Controller::list ...");
    	List<${context.entityName}> list;
        try {
            list = ${context.bizEnName}Service.query${context.entityName}List(req);
		} catch (RuntimeException e) {
			e.printStackTrace();
			return new ResponseResult(Const.RESPONE_CODE_UNKNOWN, "fail");
		}
	    return new ResponseResult(200, "success" , BaseService.packPageResult(list));

    }

    @ApiOperation(value = "按${context.bizEnName}Id获取${context.bizChName}详情")
    @PostMapping("detail")
    public ResponseResult detail(@RequestBody ${context.entityName} reqDetail) {
    	log.debug("${context.bizEnBigName}Controller::detail ...");
    	${context.entityName} entity = null;
        try {
            entity = ${context.bizEnName}Service.query${context.entityName}ById(reqDetail);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new ResponseResult(Const.RESPONE_CODE_UNKNOWN, "fail");
        }
		return new ResponseResult(200, "success" , entity);
    }

    @ApiOperation(value = "新增${context.bizChName}")
    @PostMapping("add")
    public ResponseResult add(@RequestBody ${context.entityName} reqAdd) {
        log.debug("${context.bizEnBigName}Controller::add ...");
        try {
            ${context.bizEnName}Service.insert${context.entityName}(reqAdd);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new ResponseResult(Const.RESPONE_CODE_UNKNOWN, "fail");
        }
        return new ResponseResult(200, "success");
    }

    @ApiOperation(value = "按id删除${context.bizChName}")
    @PostMapping("delete")
    public ResponseResult delete(@RequestBody ${context.entityName} reqDelete) {
        log.debug("${context.bizEnBigName}Controller::delete ...");
        try {
            ${context.bizEnName}Service.delete${context.entityName}ById(reqDelete);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new ResponseResult(Const.RESPONE_CODE_UNKNOWN, "fail");
        }
        return new ResponseResult(200, "success");
    }

    @ApiOperation(value = "按id修改${context.bizChName}")
    @PostMapping("edit")
    public ResponseResult edit(@RequestBody ${context.entityName} reqEdit) {
        log.debug("${context.bizEnBigName}Controller::edit ...");
        try {
            ${context.bizEnName}Service.update${context.entityName}ById(reqEdit);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new ResponseResult(Const.RESPONE_CODE_UNKNOWN, "fail");
        }
        return new ResponseResult(200, "success");
    }

}
