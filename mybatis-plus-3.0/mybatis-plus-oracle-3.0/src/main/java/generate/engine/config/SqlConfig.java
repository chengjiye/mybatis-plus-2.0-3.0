package generate.engine.config;


import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import lombok.Data;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * 全局配置
 *
 * @author Cheng JiYe
 * @date 2020/3/22 18:30
 */
@Data
public class SqlConfig {
    private String sqlPathTemplate;

    private ContextConfig contextConfig;

    private Connection connection;

    private String parentMenuName;

    private List<Menu> menus = new ArrayList(6);

    public void init() {

        this.sqlPathTemplate = "\\src\\main\\java\\{}.sql";

        if (parentMenuName == null) {
            return;
        }

        // 根据父菜单查询数据库中的pcode和pcodes
        String[] pcodeAndPcodes = getPcodeAndPcodes();
        if (pcodeAndPcodes == null) {
            System.err.println("父级菜单名称输入有误!!!!");
            return;
        }

        // 业务菜单
        Menu menu = new Menu();
        menu.setId(IdWorker.getId());
        menu.setCode(contextConfig.getBizEnName());
        menu.setPcode(pcodeAndPcodes[0]);
        menu.setPcodes(pcodeAndPcodes[1] + "[" + pcodeAndPcodes[0] + "],");
        menu.setName(contextConfig.getBizChName());
        menu.setIcon("");
        menu.setUrl("/" + contextConfig.getBizEnName());
        menu.setNum(99);

        if (parentMenuName.equals("顶级")) {
            menu.setLevels(1);
        } else {
            menu.setLevels(2);
        }
        menu.setIsmenu(PermissionTypeEnum.MENU.getCode());
        menu.setStatus(1);
        menu.setIsopen(0);
        menus.add(menu);

        // 列表
        Menu list = createSubMenu(menu);
        list.setCode(contextConfig.getBizEnName() + "_list");
        list.setName(contextConfig.getBizChName() + "列表");
        list.setUrl("/" + contextConfig.getBizEnName() + "/list");
        menus.add(list);

        // 添加
        Menu add = createSubMenu(menu);
        add.setCode(contextConfig.getBizEnName() + "_add");
        add.setName(contextConfig.getBizChName() + "添加");
        add.setUrl("/" + contextConfig.getBizEnName() + "/add");
        menus.add(add);

        // 更新
        Menu update = createSubMenu(menu);
        update.setCode(contextConfig.getBizEnName() + "_update");
        update.setName(contextConfig.getBizChName() + "更新");
        update.setUrl("/" + contextConfig.getBizEnName() + "/update");
        menus.add(update);

        // 删除
        Menu delete = createSubMenu(menu);
        delete.setCode(contextConfig.getBizEnName() + "_delete");
        delete.setName(contextConfig.getBizChName() + "删除");
        delete.setUrl("/" + contextConfig.getBizEnName() + "/delete");
        menus.add(delete);

        // 详情
        Menu detail = createSubMenu(menu);
        detail.setCode(contextConfig.getBizEnName() + "_detail");
        detail.setName(contextConfig.getBizChName() + "详情");
        detail.setUrl("/" + contextConfig.getBizEnName() + "/detail");
        menus.add(detail);
    }

    private Menu createSubMenu(Menu parentMenu) {
        Menu menu = new Menu();
        menu.setId(IdWorker.getId());
        menu.setPcode(parentMenu.getCode());
        menu.setPcodes(parentMenu.getPcodes() + "[" + parentMenu.getCode() + "],");
        menu.setIcon("");
        menu.setNum(99);
        menu.setLevels(parentMenu.getLevels() + 1);
        menu.setIsmenu(PermissionTypeEnum.BUTTON.getCode());
        menu.setStatus(1);
        menu.setIsopen(0);
        return menu;
    }

    public String[] getPcodeAndPcodes() {
        if (parentMenuName.equals("顶级")) {
            return new String[]{"0", ""};
        }
        return null;
    }

}
