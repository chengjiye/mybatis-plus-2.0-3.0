package generate.engine.config;

/**
 * @author Cheng JiYe
 * @date 2020/3/22 18:30
 */
public enum PermissionTypeEnum {
    /**
     * 菜单
     */
    MENU(1, "菜单"),
    /**
     * 目录
     */
    DIRECTORY(2, "目录"),
    /**
     * 按钮
     */
    BUTTON(3, "按钮");

    int code;
    String message;

    PermissionTypeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String valueOf(Integer status) {
        if (status == null) {
            return "";
        } else {
            for (PermissionTypeEnum s : PermissionTypeEnum.values()) {
                if (status.equals(s.getCode())) {
                    return s.getMessage();
                }
            }
            return "";
        }
    }
}
