# mybatis-plus-2.0-3.0

#### 介绍
本项目收集了 mybatis-plus-2.X 和 mybatis-plus-3.X的 oracle和mysql的逆向工程，自动生成代码

###  **个人声明** 

本项目不是原创，但是本人也是经过了大量的修改和完善。因为个人能力有限，如有不足，请谅解，谢谢！



#### 软件架构
软件架构说明



> 1、由于每个人要生成的模板代码不一致，可以根据需要自行定制模板代码




> 2、用到的工具



>  apache-maven-3.5.4
> 
>  jdk-1.8
> 
>  lombok -1.18.8



#### 使用说明

1、 在使用开发工具为 ideaIU-2019.1.3


> ideaIU-2019.1.3百度网盘下载地址
> 
> 链接：https://pan.baidu.com/s/15KaVFx4Ugdu1iaTn_Siiww 
> 
> 提取码：zwh2 
> 
> 因为涉及到idea的版权问题，支持正版，如何破解，请自行度娘


2、 idea要安装 lombok 插件，很简单，请自行百度


3、 因为maven仓库并没有oracle的jar包，所以需要手动把oracle的驱动包添加到maven仓库中去（很简单，请自行百度）

    安装本地jar包到本地maven仓库命令
    准备工作 1>配置jdk-1.8环境变量（很简单，请自行百度）
             2>配置maven环境变量 （很简单，请自行百度）
     具体安装命令：
     mvn install:install-file -Dfile=D:\svn-projects\json-lib-2.4.jar -DgroupId=net.sf.json-lib -DartifactId=json-lib -Dversion=2.4 -Dpackaging=jar
     mvn install:install-file -Dfile=本地jar位置 -DgroupId=groupId组织id  -DartifactId=artifactId   -Dversion=version -Dpackaging=jar


4、ojdbc7百度网盘下载地址：

>     链接：https://pan.baidu.com/s/1ErH2HT7iujL3hCjROGJtkg 
>     提取码：ofeq


5、把生成好的代码集成到自己的项目中去

注意事项：

1、如果要集成到自己的项目中，需要注意项目中 的 mapper 文件的扫描路径问题，如果放到 resource 的 mapper/station目录下
![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/180005_668ac240_5133785.png "屏幕截图.png")
需要这样配置 注意匹配规则，最好是  *.xml ,以免匹配不到

![输入图片说明](https://images.gitee.com/uploads/images/2020/0323/010940_6a4ac774_5133785.png "屏幕截图.png")

如果 mybatis-plus.mapper-locations=classpath:/mapping/**Mapper.xml 这样配置，只要访问接口就会出现无效的绑定声明

![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/175803_6b4c9447_5133785.png "屏幕截图.png")

2、如果xml文件是直接放到resource 的 mapper 目录下，这样配置就行，如下图所示
mybatis-plus.mapper-locations=classpath:/mapping/*.xml 建议这样匹配，本人就这样遇到过，团队成员的mapper接口定义不规范，导致接口扫描不到

![输入图片说明](https://images.gitee.com/uploads/images/2020/0323/011135_af30b2fd_5133785.png "屏幕截图.png")

3、特别注意：如果是oracle 的逆向工程，那么，生成实体类模板的主键id要自增的话，类型必须为 ，并且相应的数据库每张表都要建立独立的序列以此来实现oracle数据库主键的自增
![输入图片说明](https://images.gitee.com/uploads/images/2020/0408/110128_23e8f0a3_5133785.png "屏幕截图.png")
5、代码生成简单说明

生成代码入口

![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/180912_f134888f_5133785.png "屏幕截图.png")
定制模板代码
![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/181309_fde9d5eb_5133785.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/181457_81721974_5133785.png "屏幕截图.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
