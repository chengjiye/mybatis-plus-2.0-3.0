package generate;

import generate.action.config.WebGeneratorConfig;
import generate.action.model.GenQo;

/**
 * 一键生成后台代码
 *
 * @author Cheng JiYe
 * @date 2020/3/22 18:30
 */
public class Generator {
    public static void main(String[] args) {
        GenQo genQo = new GenQo();
        // 数据库url
        genQo.setUrl("jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=GMT%2B8");
        // 数据库表名
        String tableName = "user";
        genQo.setTableName(tableName);
        //数据库用户名
        genQo.setUsername("root");
        // 数据库密码
        genQo.setPassword("123456");
        //生成文件的地址
        genQo.setProjectPath("D:\\mybatis-generate\\" + tableName);
        // 作者
        genQo.setAuthor("chengjiye");
        // 业务名称
        genQo.setBizName("收费站基础信息表");
        //设置目录
        genQo.setCatalogue("user");
        // 项目的包
        genQo.setProjectPackage("");
        //设置模块包（就是在cotroller,service,mapper层的一个目录或者package）
        genQo.setModuleName("user");
        //是否生成Controller代码
        genQo.setControllerSwitch(true);
        // 是否生成dao代码
        genQo.setDaoSwitch(true);
        //是否生成Service代码
        genQo.setServiceSwitch(true);
        // 是否生成Entity代码
        genQo.setEntitySwitch(true);
        // 是否生成sql语句
        genQo.setSqlSwitch(false);
        //是否生成主页
        genQo.setIndexPageSwitch(false);
        // 是否生成添加页面
        genQo.setAddPageSwitch(false);
        // 是否生成修改页面
        genQo.setEditPageSwitch(false);
        //是否要生成js
        genQo.setJsSwitch(false);
        //是否生成info js
        genQo.setInfoJsSwitch(false);
        //忽略表前缀
        genQo.setIgnoreTabelPrefix("t_");
        // 核心模块的包
        genQo.setCorePackage("");
        WebGeneratorConfig webGeneratorConfig = new WebGeneratorConfig(genQo);
        webGeneratorConfig.doMpGeneration(genQo);
        webGeneratorConfig.doGeneration();
        MybatisGenerator.editFilePath(genQo.getProjectPath(), genQo.getProjectPath());
        MybatisGenerator.deleteDirectory(genQo.getProjectPath() + "\\src");

    }
}
