package generate.engine.config;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 控制器模板生成的配置
 *
 * @author Cheng JiYe
 * @date 2020/3/22 18:30
 */
@Data
public class ControllerConfig {
    private ContextConfig contextConfig;

    private String controllerPathTemplate;
    /**
     * 包名称
     */
    private String packageName;
    /**
     * 作者
     */
    private String author;
    /**
     * 所引入的包
     */
    private List<String> imports;

    public void init() {
        ArrayList<String> imports = new ArrayList();
        imports.add(contextConfig.getCoreBasePackage() + ".base.restful.JsonResult");
        imports.add(contextConfig.getCoreBasePackage() + ".constant.state.StatusCodeEnum");
        imports.add("org.springframework.stereotype.Controller");
        imports.add("org.springframework.web.bind.annotation.RequestMapping");
        imports.add("org.springframework.web.bind.annotation.RequestMethod");
        imports.add("org.springframework.web.bind.annotation.RequestParam");
        imports.add("org.springframework.web.bind.annotation.ResponseBody");
        imports.add("org.springframework.web.bind.annotation.PathVariable");
        imports.add("org.springframework.ui.Model");
        imports.add("org.springframework.beans.factory.annotation.Autowired");
        imports.add(contextConfig.getProPackage() + ".core.common.controller.BaseController");
        imports.add(contextConfig.getProPackage() + ".core.log.LogObjectHolder");
        imports.add(contextConfig.getModelPackageName() + "." + contextConfig.getEntityName());
        imports.add(contextConfig.getProPackage() + ".modular." + contextConfig.getModuleName() + ".service" + ".I" + contextConfig.getEntityName() + "Service");
        this.imports = imports;
        this.packageName = contextConfig.getProPackage() + ".modular." + contextConfig.getModuleName() + ".controller";
        this.controllerPathTemplate = "\\src\\main\\java\\" + contextConfig.getProPackage().replaceAll("\\.", "\\\\") + "\\modular\\" + contextConfig.getModuleName() + "\\controller\\{}Controller.java";
        this.author = contextConfig.getAuthor();
    }
}
