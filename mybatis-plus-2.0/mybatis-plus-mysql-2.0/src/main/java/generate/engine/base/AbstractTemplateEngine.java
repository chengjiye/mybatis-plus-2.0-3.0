package generate.engine.base;

import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import generate.engine.config.*;
import lombok.Data;

/**
 * 模板生成父类
 *
 * @author Cheng JiYe
 * @date 2020/3/22 18:30
 */
@Data
public class AbstractTemplateEngine {

    /**
     * 全局配置
     */
    protected ContextConfig contextConfig;

    /**
     * 控制器的配置
     */
    protected ControllerConfig controllerConfig;

    /**
     * 页面的控制器
     */
    protected PageConfig pageConfig;

    /**
     * Dao配置
     */
    protected DaoConfig daoConfig;

    /**
     * Service配置
     */
    protected ServiceConfig serviceConfig;

    /**
     * sql配置
     */
    protected SqlConfig sqlConfig;

    /**
     * 表的信息
     */
    protected TableInfo tableInfo;

    public void initConfig() {
        if (this.contextConfig == null) {
            this.contextConfig = new ContextConfig();
        }
        if (this.controllerConfig == null) {
            this.controllerConfig = new ControllerConfig();
        }
        if (this.pageConfig == null) {
            this.pageConfig = new PageConfig();
        }
        if (this.daoConfig == null) {
            this.daoConfig = new DaoConfig();
        }
        if (this.serviceConfig == null) {
            this.serviceConfig = new ServiceConfig();
        }
        if (this.sqlConfig == null) {
            this.sqlConfig = new SqlConfig();
        }

        this.contextConfig.init();
        this.controllerConfig.setContextConfig(this.contextConfig);
        this.controllerConfig.setControllerPathTemplate("classpath:gentemplate/Controller.java.btl");
        this.controllerConfig.init();
        this.serviceConfig.setContextConfig(this.contextConfig);
        this.serviceConfig.init();
        this.daoConfig.setContextConfig(this.contextConfig);
        this.daoConfig.init();
        this.pageConfig.setContextConfig(this.contextConfig);
        this.pageConfig.init();
        this.sqlConfig.setContextConfig(this.contextConfig);
        this.sqlConfig.init();
    }

}
