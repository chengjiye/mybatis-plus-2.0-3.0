package generate.action.config;

import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import generate.MybatisGenerator;
import generate.action.model.GenQo;
import generate.engine.GenTemplateEngine;
import generate.engine.SimpleTemplateEngine;
import generate.engine.config.ContextConfig;
import generate.engine.config.SqlConfig;

import java.util.List;

/**
 * 代码生成的抽象配置
 *
 * @author Cheng JiYe
 * @date 2020/3/22 18:30
 */
public abstract class AbstractGeneratorConfig {
    /**
     * mybatis-plus代码生成器配置
     */
    GlobalConfig globalConfig = new GlobalConfig();

    DataSourceConfig dataSourceConfig = new DataSourceConfig();


    StrategyConfig strategyConfig = new StrategyConfig();

    PackageConfig packageConfig = new PackageConfig();

    TableInfo tableInfo = null;

    /**
     * 代码生成器配置
     */
    ContextConfig contextConfig = new ContextConfig();
    SqlConfig sqlConfig = new SqlConfig();


    protected abstract void config();

    public void init() {
        config();

        packageConfig.setService(contextConfig.getProPackage() + ".modular." + contextConfig.getModuleName() + ".service");
        packageConfig.setServiceImpl(contextConfig.getProPackage() + ".modular." + contextConfig.getModuleName() + ".service.impl");

        //controller没用掉,生成之后会自动删掉
        packageConfig.setController("TTT");

        if (!contextConfig.getEntitySwitch()) {
            packageConfig.setEntity("TTT");
        }

        if (!contextConfig.getDaoSwitch()) {
            packageConfig.setMapper("TTT");
            packageConfig.setXml("TTT");
        }

        if (!contextConfig.getServiceSwitch()) {
            packageConfig.setService("TTT");
            packageConfig.setServiceImpl("TTT");
        }

    }

    /**
     * 删除不必要的代码
     */
    public void destory() {
        String outputDir = globalConfig.getOutputDir() + "/TTT";
    }

    public AbstractGeneratorConfig() {
    }

    public void doMpGeneration(GenQo genQo) {
        init();

        TemplateConfig template = new TemplateConfig();
        template.setEntity(contextConfig.getTemplatePrefixPath() + "/entity.java.vm");
        template.setMapper(contextConfig.getTemplatePrefixPath() + "/mapper.java.vm");
        template.setXml(contextConfig.getTemplatePrefixPath() + "/mapper.xml.vm");
        template.setService(contextConfig.getTemplatePrefixPath() + "/service.java.vm");
        template.setServiceImpl(contextConfig.getTemplatePrefixPath() + "/serviceImpl.java.vm");
        template.setController(contextConfig.getTemplatePrefixPath() + "/controller.java.vm");

        MybatisGenerator mybatisGenerator = new MybatisGenerator(genQo);
        mybatisGenerator.setGlobalConfig(globalConfig);
        mybatisGenerator.setDataSource(dataSourceConfig);
        mybatisGenerator.setStrategy(strategyConfig);
        mybatisGenerator.setPackageInfo(packageConfig);
        mybatisGenerator.setTemplate(template);
        mybatisGenerator.execute();
        destory();

        // 获取table信息,用于代码生成
        List<TableInfo> tableInfoList = mybatisGenerator.getConfig().getTableInfoList();
        if (tableInfoList != null && tableInfoList.size() > 0) {
            this.tableInfo = tableInfoList.get(0);
        }

    }

    public void doGeneration() {
        GenTemplateEngine gentemplateEngine = new SimpleTemplateEngine();
        gentemplateEngine.setContextConfig(contextConfig);
        sqlConfig.setConnection(dataSourceConfig.getConn());
        gentemplateEngine.setSqlConfig(sqlConfig);
        gentemplateEngine.setTableInfo(tableInfo);
        gentemplateEngine.start();
    }
}
