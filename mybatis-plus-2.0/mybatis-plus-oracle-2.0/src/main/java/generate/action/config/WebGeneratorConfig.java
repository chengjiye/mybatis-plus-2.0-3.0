package generate.action.config;

import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.converts.OracleTypeConvert;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import generate.action.model.GenQo;
import generate.engine.config.ToolUtil;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 默认的代码生成的配置
 *
 * @author Cheng JiYe
 * @date 2020/3/22 18:30
 */
public class WebGeneratorConfig extends AbstractGeneratorConfig {
    private GenQo genQo;


    public WebGeneratorConfig(GenQo genQo) {
        this.genQo = genQo;
    }

    @Override
    protected void config() {
        String dates = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());

        // region 数据库配置
        dataSourceConfig.setDbType(DbType.ORACLE);
        dataSourceConfig.setDriverName("oracle.jdbc.driver.OracleDriver");
        dataSourceConfig.setUsername(genQo.getUsername());
        dataSourceConfig.setPassword(genQo.getPassword());
        dataSourceConfig.setUrl(genQo.getUrl());

        dataSourceConfig.setTypeConvert(new OracleTypeConvert() {
            @Override
            public DbColumnType processTypeConvert(String fieldType) {
                System.out.println("转换类型：" + fieldType);
                //number转换成Boolean
                if (fieldType.toLowerCase().contains("number")) {
                    return DbColumnType.LONG;
                }
                //将数据库中datetime转换成date
                if (fieldType.toLowerCase().contains("date")) {
                    return DbColumnType.DATE;
                }
                return super.processTypeConvert(fieldType);
            }
        });
        // endregion

        // region 全局配置
        globalConfig.setOutputDir(genQo.getProjectPath() + File.separator + "src" + File.separator + "main" + File.separator + "java");
        globalConfig.setFileOverride(true);
        globalConfig.setEnableCache(false);
        globalConfig.setBaseResultMap(true);
        globalConfig.setBaseColumnList(true);
        globalConfig.setOpen(false);
        globalConfig.setAuthor(genQo.getAuthor());
        contextConfig.setAuthor(genQo.getAuthor());
        contextConfig.setCatalogue(genQo.getCatalogue());
        contextConfig.setDateController(dates);
        contextConfig.setProPackage(genQo.getProjectPackage());
        contextConfig.setCoreBasePackage(genQo.getCorePackage());
        // endregion

        // region 生成策略
        if (genQo.getIgnoreTabelPrefix() != null) {
            strategyConfig.setTablePrefix(new String[]{genQo.getIgnoreTabelPrefix()});
        }
        strategyConfig.setInclude(new String[]{genQo.getTableName()});
        strategyConfig.setNaming(NamingStrategy.underline_to_camel);
        packageConfig.setParent(null);
        packageConfig.setEntity(genQo.getProjectPackage() + ".modular.system.model");
        packageConfig.setMapper(genQo.getProjectPackage() + ".modular.system.dao");
        packageConfig.setXml(genQo.getProjectPackage() + ".modular.system.dao.mapping");
        // endregion

        // region 业务代码配置
        contextConfig.setBizChName(genQo.getBizName());
        contextConfig.setModuleName(genQo.getModuleName());
        // 写自己项目的绝对路径
        contextConfig.setProjectPath(genQo.getProjectPath());
        if (ToolUtil.isEmpty(genQo.getIgnoreTabelPrefix())) {
            String entityName = ToolUtil.toCamelCase(genQo.getTableName());
            contextConfig.setEntityName(ToolUtil.firstCharToUpperCase(entityName));
            contextConfig.setBizEnName(ToolUtil.firstCharToLowerCase(entityName));
        } else {
            String entiyName = ToolUtil.toCamelCase(ToolUtil.removePrefix(genQo.getTableName(), genQo.getIgnoreTabelPrefix()));
            contextConfig.setBizChNameTmp(ToolUtil.firstCharToUpperCase(entiyName));
            contextConfig.setEntityName(ToolUtil.firstCharToUpperCase(entiyName) + "Entity");
            contextConfig.setBizEnName(ToolUtil.firstCharToLowerCase(ToolUtil.toCamelCase(ToolUtil.removePrefix(genQo.getTableName(), genQo.getIgnoreTabelPrefix()))));
        }
        // 这里写已有菜单的名称,当做父节点
        sqlConfig.setParentMenuName(genQo.getParentMenuName());
        // endregion

        //region  mybatis-plus 生成器开关
        contextConfig.setEntitySwitch(genQo.getEntitySwitch());
        contextConfig.setDaoSwitch(genQo.getDaoSwitch());
        contextConfig.setServiceSwitch(genQo.getServiceSwitch());
        //endregion

        //region 代码生成器开关
        contextConfig.setControllerSwitch(genQo.getControllerSwitch());
        contextConfig.setIndexPageSwitch(genQo.getIndexPageSwitch());
        contextConfig.setAddPageSwitch(genQo.getAddPageSwitch());
        contextConfig.setEditPageSwitch(genQo.getEditPageSwitch());
        contextConfig.setJsSwitch(genQo.getJsSwitch());
        contextConfig.setInfoJsSwitch(genQo.getInfoJsSwitch());
        contextConfig.setSqlSwitch(genQo.getSqlSwitch());
        //endregion
    }
}
