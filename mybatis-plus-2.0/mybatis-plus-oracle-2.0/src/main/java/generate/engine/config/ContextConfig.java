package generate.engine.config;

import lombok.Data;

import static generate.engine.config.ToolUtil.firstCharToUpperCase;

/**
 * 全局配置
 *
 * @author Cheng JiYe
 * @date 2020/3/22 18:30
 */
@Data
public class ContextConfig {

    private String templatePrefixPath = "gentemplate";
    /**
     * 模板输出的项目目录
     */
    private String projectPath = "D:\\projects";
    /**
     * 业务名称
     */
    private String bizChName;

    /**
     * 临时业务名称
     */
    private String bizChNameTmp;
    /**
     * 业务英文名称
     */
    private String bizEnName;
    /**
     * 业务英文名称(大写)
     */
    private String bizEnBigName;
    /**
     * 开发人员
     */
    private String author;
    /**
     * 模块名称
     */
    private String moduleName = "system";

    private String proPackage = "cn.mycs.cms";
    private String coreBasePackage = "cn.mycs.core";
    /**
     * model的包名
     */
    private String modelPackageName = "cn.mycs.cms.modular";
    /**
     * model的dao
     */
    private String modelMapperPackageName = "cn.mycs.cms.modular.system.dao";
    /**
     * 实体的名称
     */
    private String entityName;

    /**
     * 实体的名称
     */
    private String entityNameCreateReq;

    /**
     * 是否生成控制器代码开关
     */
    private Boolean controllerSwitch = true;
    /**
     * 主页
     */
    private Boolean indexPageSwitch = true;
    /**
     * 添加页面
     */
    private Boolean addPageSwitch = true;
    /**
     * 编辑页面
     */
    private Boolean editPageSwitch = true;
    /**
     * js
     */
    private Boolean jsSwitch = true;
    /**
     * 详情页面js
     */
    private Boolean infoJsSwitch = true;
    /**
     * dao
     */
    private Boolean daoSwitch = true;
    /**
     * service
     */
    private Boolean serviceSwitch = true;
    /**
     * 生成实体的开关
     */
    private Boolean entitySwitch = true;
    /**
     * 生成sql的开关
     */
    private Boolean sqlSwitch = true;

    /**
     * 目录
     */
    private String catalogue;

    /**
     * controller 类时间
     */
    private String dateController;

    public void init() {
        if (entityName == null) {
            entityName = bizEnBigName;
        }
        modelPackageName = proPackage + "." + "system.model";
        modelMapperPackageName = proPackage + "." + "system.dao";
    }


    public void setBizEnName(String bizEnName) {
        this.bizEnName = bizEnName;
        this.bizEnBigName = firstCharToUpperCase(this.bizEnName);
    }

}
