package generate;

import generate.action.config.WebGeneratorConfig;
import generate.action.model.GenQo;


/**
 * 一键生成后台代码
 *
 * @author Cheng JiYe
 * @date 2020/3/22 18:30
 */
public class Generator {
    public static void main(String[] args) {

        GenQo genQo = new GenQo();
        // 数据库url
        genQo.setUrl("jdbc:oracle:thin:@//localhost:1521/orclpdb");
        // 数据库表名

        String tableName = "TB_BASE_STATION";
        genQo.setTableName(tableName);

        //数据库用户名
        genQo.setUsername("jeesite");
        // 数据库密码
        genQo.setPassword("jeesite");
        //生成文件的地址
        genQo.setProjectPath("D:\\mybatis-generate\\" + tableName);
        // 作者

        genQo.setAuthor("chengjiye");
        // 业务名称
        genQo.setBizName("收费站基础信息表");
        //设置目录
        genQo.setCatalogue("station");


        // 项目的包
        genQo.setProjectPackage("");
        genQo.setModuleName("station");
        //是否生成Controller代码
        genQo.setControllerSwitch(true);
        // 是否生成dao代码
        genQo.setDaoSwitch(true);
        //是否生成Service代码
        genQo.setServiceSwitch(true);
        // 是否生成Entity代码
        genQo.setEntitySwitch(true);
        // 是否生成sql语句
        genQo.setSqlSwitch(false);
        //是否生成主页
        genQo.setIndexPageSwitch(false);
        // 是否生成添加页面
        genQo.setAddPageSwitch(false);
        // 是否生成修改页面
        genQo.setEditPageSwitch(false);
        //是否要生成js
        genQo.setJsSwitch(false);
        //是否生成info js
        genQo.setInfoJsSwitch(false);
        //忽略表前缀(例如，表名为TB_BASE_USER,生成的类名为BaseUser,如果不设置，则生成的类名为 TbBaseUser)
        genQo.setIgnoreTabelPrefix("TB_");
        // 核心模块的包
        genQo.setCorePackage("");
        WebGeneratorConfig webGeneratorConfig = new WebGeneratorConfig(genQo);
        webGeneratorConfig.doMpGeneration(genQo);
        webGeneratorConfig.doGeneration();
        MybatisGenerator.editFilePath(genQo.getProjectPath(), genQo.getProjectPath());
        MybatisGenerator.deleteDirectory(genQo.getProjectPath() + "\\src");

    }
}
